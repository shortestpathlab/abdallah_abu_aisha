//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_HEADER_H
#define PTVFTCSA2_HEADER_H

#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <iomanip>
#include "Station.h"
#include "Connection.h"
#include "Trip.h"
#include "Label.h"
#include "Tempo_Label.h"
#include "Segment.h"

using namespace std;

// functions

int bin_search_next(const vector<Label> &vec, const int &val);

int station_no(const string &station_name, const vector<Station> &stations);

int search_arrival(const vector<int> &trip_connections, const int &val, const vector<Connection> &connections);

int first_con(const vector<Connection> &vec, const int &val);

bool sort_con(const Connection &con1, const Connection &con2);

void create_labels(const int &source, const vector<Station> &stations, const vector<Connection> &connections,
                   const int &trips_no, vector<vector<vector<Label>>> &Labels);

vector<Segment> find_path(const int &source, const int &target, const int &start_time,
                          const vector<vector<vector<Label>>> &labels, const vector<Trip> &trips,
                          const vector<int> &con_ind, const vector<Connection> &connections,
                          const vector<Station> &stations);

void print_path(const vector<Segment> &path, const vector<Station> &stations, const vector<string> &services);

double compute_size(const vector<vector<vector<Label>>> &labels);

#endif //PTVFTCSA2_HEADER_H
