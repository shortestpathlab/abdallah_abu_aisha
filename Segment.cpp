//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#include "Segment.h"

Segment::Segment(){

}

Segment::~Segment(){

}

Segment::Segment(int from_val, int to_val, int departure_val, int arrival_val, int service_val) {
    from = from_val;
    to = to_val;
    departure = departure_val;
    arrival = arrival_val;
    service = service_val;
}