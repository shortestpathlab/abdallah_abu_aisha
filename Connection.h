//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_CONNECTION_H
#define PTVFTCSA2_CONNECTION_H

#include <iostream>

using namespace std;

class Connection {
public:
    int from;
    int to;
    int departure;
    int arrival;
    int trip;

    Connection();
    Connection(int from_val, int to_val, int departure_val, int arrival_val, int trip_val);
    ~Connection();
};


#endif //PTVFTCSA2_CONNECTION_H
