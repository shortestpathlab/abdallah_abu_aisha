/// First transfer CPD with CSA; modified implementation considering concise
/// representation of trips and labels (12/09/22)
// "Trip" class is created

#include "main.h"

// *********************************************************************************************************************

int main() {
    auto start0 = chrono::steady_clock::now();
    //<editor-fold desc="Building database">
    // Defining stations and connections

    // Defining connections/edges

    vector<vector<int>> data(5);

    ifstream in_file;
    int stop_id3;
    int seq3;
    int arr;
    int dep3;
    int service;

    in_file.open("../connections.txt");

    if (!in_file) {
        cerr << "Problem Opening File" << endl;
        return 1;
    }

    string line3;
    while (!in_file.eof()) {
        getline(in_file, line3);
        in_file >> stop_id3;
        in_file >> seq3;
        in_file >> arr;
        in_file >> dep3;
        in_file >> service;
        data.at(0).push_back(stop_id3);
        data.at(1).push_back(seq3);
        data.at(2).push_back(arr);
        data.at(3).push_back(dep3);
        data.at(4).push_back(service);
    }

    in_file.close();

    vector<Trip> all_trips;
    int trip_no{0};
    Trip first_trip;
    first_trip.service = data.at(4).at(0);
    all_trips.push_back(first_trip);
    vector<Connection> all_connections;
    Connection first_con(data.at(0).at(0), data.at(0).at(1), data.at(3).at(0), data.at(2).at(1), trip_no);
    all_connections.push_back(first_con);

    for (int i{1}; i < data.at(0).size() - 1; i++) {
        if (data.at(1).at(i) < data.at(1).at(i + 1)) {
            Connection edge;
            edge.from = data.at(0).at(i);
            edge.to = data.at(0).at(i + 1);
            edge.departure = data.at(3).at(i);
            edge.arrival = data.at(2).at(i + 1);
            edge.trip = trip_no;
            all_connections.push_back(edge);
        } else {
            trip_no++;
            Trip trip;
            trip.service = data.at(4).at(i + 1);
            all_trips.push_back(trip);
        }
    }

    // sort connections based on "departure" (as required by CSA) then "arrival"
    sort(all_connections.begin(), all_connections.end(), sort_con);

    // defining connection numbers for trips after sorting
    for (int c{0}; c < all_connections.size(); c++) {
        all_trips[all_connections[c].trip].connections.push_back(c);
    }

    // Defining station names, transfer times

    vector<string> names;
    ifstream in_file2;
    string line2;

    in_file2.open("../station_names.txt");
    if (!in_file2) {
        cerr << "Problem Opening File" << endl;
        return 1;
    }

    while (!in_file2.eof()) {
        getline(in_file2, line2);
        names.push_back(line2);
    }
    in_file2.close();

    // Building station class and all_stations vector

    vector<Station> all_stations(names.size());
    for (int i{0}; i < all_stations.size(); i++) {
        all_stations.at(i).name = names.at(i);
        all_stations.at(i).transfer_time = 4;
    }

    for (int c{0}; c < all_connections.size(); c++) {
        all_stations.at(all_connections.at(c).from).depEvents.push_back(c);
    }

    // sorting departure events in decreasing order for each station for the preprocessing
    for (int s{0}; s < all_stations.size(); s++) {
        reverse(all_stations.at(s).depEvents.begin(), all_stations.at(s).depEvents.end());
    }

    // creating the list of all services names
    vector<string> all_services;
    ifstream in_file3;
    string line4;

    in_file3.open("../services.txt");
    if (!in_file3) {
        cerr << "Problem Opening File" << endl;
        return 1;
    }

    while (!in_file3.eof()) {
        getline(in_file3, line4);
        all_services.push_back(line4);
    }
    in_file3.close();

    // Delete no-longer needed vectors/data
    names.clear();
    names.shrink_to_fit();
    data.clear();
    data.shrink_to_fit();

    //</editor-fold>

    // connection-time index vector (hash table)
    vector<int> con_ind(all_connections.back().departure + 1, -1);
    for (int i{0}; i < all_connections.size(); i++) {
        if (i == 0 || all_connections.at(i).departure != all_connections.at(i - 1).departure)
            con_ind.at(all_connections.at(i).departure) = i;
    }
    for (int i{static_cast<int>(con_ind.size() - 2)}; i >= 0; i--) {
        if (con_ind.at(i) == -1) {
            con_ind.at(i) = con_ind.at(i + 1);
        }
    }


    // apply CSA and creating labels (Preprocessing)
    vector<vector<vector<Label>>> all_labels(all_stations.size(), vector<vector<Label>>(all_stations.size()));
//    vector<vector<Label>> all_labels_comp;

    for (int i{0}; i < all_stations.size(); i++) {
        create_labels(i, all_stations, all_connections, trip_no, all_labels);
    }

    auto end0 = chrono::steady_clock::now();
    auto elapsed0 = chrono::duration_cast<chrono::milliseconds>(end0 - start0).count();
    cout << "Preprocessing Time (ms): " << elapsed0 << endl;

    cout << "Total Preprocessing space: " << compute_size(all_labels) << endl;

// *********************************************************************************************************************

//  Finding average query time through running 1000 random queries
    vector<vector<int>> queries1(3);

    ifstream in_file4;
    int source;
    int target;
    int start_time;

    in_file4.open("../queries.txt");

    if (!in_file4) {
        cerr << "Problem Opening File" << endl;
        return 1;
    }

    string line1;
    while (!in_file4.eof()) {
        getline(in_file4, line1);
        in_file4 >> source;
        in_file4 >> target;
        in_file4 >> start_time;
        queries1.at(0).push_back(source);
        queries1.at(1).push_back(target);
        queries1.at(2).push_back(start_time);
    }

    in_file4.close();

    vector<Segment> pathX;
    int total_path_lengths{0};

    vector<int> query_time;
    for (int q{0}; q < queries1.at(0).size(); q++) {
        auto start = chrono::steady_clock::now();
        pathX = find_path(queries1.at(0).at(q), queries1.at(1).at(q), queries1.at(2).at(q), all_labels, all_trips, con_ind,
                  all_connections, all_stations);
        auto end = chrono::steady_clock::now();
        auto elapsed = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
        total_path_lengths += pathX.size();
        query_time.push_back(elapsed);
    }

    int sum{0};
    double avg{0};
    for (int i{0}; i < query_time.size(); i++) {
        sum += query_time.at(i);
    }
    avg = (sum * 1.0) / (query_time.size() * 1.0);

    cout << "Average Query Time (microsecond): " << avg / 1000 << endl << endl;
    cout << "Average Path Length (Leg/Journey): " << (total_path_lengths * 1.0) / queries1.at(0).size() << endl << endl;


// Exporting results  to a text file
//    ofstream output{"../querytime.txt"};
//    if (!output) {
//        cerr << "Error creating file" << endl;
//        return 1;
//    }
//    for (int q{0}; q < query_time.size(); q++) {
//        output << query_time.at(q) << endl;
//    }
//    output.close();

// *********************************************************************************************************************

// Answering queries
//    char queries{};
//    do {
//        string source{};
//        cout << "Enter source station name: ";
//        getline(cin, source);
//        cin.clear();
//        cin.ignore();
//        string target{};
//        cout << "Enter target station name: ";
//        getline(cin, target);
//        cin.clear();
//        cin.ignore();
//        int start_time_hh{};
//        cout << "Enter journey start time (hh): ";
//        cin >> start_time_hh;
//        cin.clear();
//        cin.ignore();
//        int start_time_mm{};
//        cout << "Enter journey start time (mm): ";
//        cin >> start_time_mm;
//        cin.clear();
//        cin.ignore();
//        int source_station = station_no(source, all_stations);
//        int target_station = station_no(target, all_stations);
//        int start_time = start_time_hh * 60 + start_time_mm;
//
//        vector<Segment> earliest_arrival_path;
//
//        auto start = chrono::steady_clock::now();
//        earliest_arrival_path = find_path(source_station, target_station, start_time, all_labels, all_trips, con_ind,
//                                          all_connections, all_stations);
//        auto end = chrono::steady_clock::now();
//        auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start).count();
//
//        print_path(earliest_arrival_path, all_stations, all_services);
//        cout << "elapsed time (µs): " << elapsed << endl;
//
//        cout << "\nMore queries? (Y/N): ";
//        cin >> queries;
//        cin.clear();
//        cin.ignore();
//    } while (queries == 'Y' or queries == 'y');
//

//  *****************************************************************************************************************


    return 0;
}

// *********************************************************************************************************************
// *********************************************************************************************************************
// *********************************************************************************************************************

// functions definitions

int first_con(const vector<Connection> &vec, const int &val) {
    int low{0};
    int high = vec.size() - 1;
    while (low <= high) {
        int mid = (low + high) / 2;
        if (vec.at(mid).departure == val && (mid == 0 || vec.at(mid - 1).departure != val))
            return mid;
        else if (vec.at(mid).departure < val)
            low = mid + 1;
        else
            high = mid - 1;
    }
    return (vec.size() - 1);
}

bool sort_con(const Connection &con1, const Connection &con2) {
    if (con1.departure < con2.departure)
        return true;
    else if (con1.departure == con2.departure && con1.arrival < con2.arrival)
        return true;
    else
        return false;
}

int station_no(const string &station_name, const vector<Station> &stations) {
    for (int s{0}; s < stations.size(); s++) {
        if (stations.at(s).name == station_name) {
            return s;
        }
    }
    return -1;
}

void create_labels(const int &source, const vector<Station> &stations, const vector<Connection> &connections,
                   const int &trips_no, vector<vector<vector<Label>>> &Labels) {
    vector<vector<Tempo_Label>> tempo(stations.size());
    for (int d{0}; d < stations[source].depEvents.size(); d++) {
        int c0 = stations[source].depEvents[d]; // first connection to start from
        int dep = connections[c0].departure;
        int trip = connections[c0].trip;
        int max_EA{0}; // for scan pruning
        int updated{0}; // for scan pruning
//        int cn{INT_MAX}; // for scan pruning (delete)
        vector<int> EA(stations.size(), INT_MAX);
        vector<int> FT(stations.size(), INT_MAX);
        vector<bool> is_reachable(trips_no + 1, false);
        vector<int> trip_station(trips_no + 1, -1);
        EA.at(source) = dep;
        is_reachable.at(trip) = true;
        trip_station.at(trip) = source;
        FT[source] = -1;
        for (int i{c0}; i < connections.size(); i++) {
            if (updated == stations.size() - 1 && connections[i].departure >= max_EA) { // for scan pruning
//                cn = i;
                break;
            }
            if (connections[i].departure > dep && connections[i].to != source && connections[i].from != source &&
                (is_reachable[connections[i].trip] ||
                 connections[i].departure -
                 stations[connections[i].from].transfer_time >=
                 EA[connections[i].from]) || i == c0) {
                if (!is_reachable[connections[i].trip]) {
                    is_reachable[connections[i].trip] = true;
                    trip_station[connections[i].trip] = connections[i].from;
                }
                if (connections[i].arrival < EA[connections[i].to]) {
                    if (EA[connections[i].to] == INT_MAX) { // for scan pruning
                        updated++;
                    }
                    if (connections[i].arrival > max_EA) { // for scan pruning
                        max_EA = connections[i].arrival;
                    }
                    EA[connections[i].to] = connections[i].arrival;
                    if (connections[i].trip == trip) {
                        FT[connections[i].to] = connections[i].to;
                    } else {
                        FT[connections[i].to] = FT[trip_station[connections[i].trip]];
                    }
                }
            }
        }
        // on-the-fly domination check (filtration) and create temporary labels
        for (int sta{0}; sta < stations.size(); sta++) {
            if (sta != source && EA[sta] != INT_MAX && (tempo[sta].empty() || EA[sta] < tempo[sta].back().EA)) {
                Tempo_Label tl(c0, dep, FT[sta], EA[sta]);
                if (tempo[sta].empty() || dep < tempo[sta].back().dep) {
                    tempo[sta].push_back(tl);
                } else if (dep == tempo[sta].back().dep) {
                    tempo[sta].back() = tl;
                }
            }
        }

        EA.clear();
        EA.shrink_to_fit();
        is_reachable.clear();
        is_reachable.shrink_to_fit();
        trip_station.clear();
        trip_station.shrink_to_fit();
        FT.clear();
        FT.shrink_to_fit();
    }
    // create permanent labels
    for (int s{0}; s < stations.size(); s++) {
        if (!tempo[s].empty()) {
            reverse(tempo[s].begin(), tempo[s].end());
        }
        for (int i{0}; i < tempo[s].size(); i++) {
            Label l(tempo[s][i].con, tempo[s][i].FT);
            Labels[source][s].push_back(l);
        }
    }
    tempo.clear();
    tempo.shrink_to_fit();
}

int bin_search_next(const vector<Label> &vec, const int &val) {
    int low{0};
    int high = vec.size() - 1;
    while (low <= high) {
        int mid = (low + high) / 2;
        if (vec[mid].connection == val)
            return mid;
        else if (vec[mid].connection > val && (mid == 0 || vec[mid - 1].connection < val))
            return mid;
        else if (vec[mid].connection > val)
            high = mid - 1;
        else
            low = mid + 1;
    }
    return -1;
}

int search_arrival(const vector<int> &trip_connections, const int &FT, const vector<Connection> &connections) {
    for (int i{0}; i < trip_connections.size(); i++) {
        if (connections[trip_connections[i]].to == FT) {
            return connections[trip_connections[i]].arrival;
        }
    }
    return -1;
}

vector<Segment> find_path(const int &source, const int &target, const int &start_time,
                          const vector<vector<vector<Label>>> &labels, const vector<Trip> &trips,
                          const vector<int> &con_ind, const vector<Connection> &connections,
                          const vector<Station> &stations) {
    vector<Segment> path;
    int trip, FT, temp_con, label_ind, current_con;
    int current_sta = source;
    int current_time = start_time;
    do {
        Segment seg;
        temp_con = con_ind[current_time];
        label_ind = bin_search_next(labels[current_sta][target], temp_con);
        current_con = labels[current_sta][target][label_ind].connection;
        FT = labels[current_sta][target][label_ind].FT;
        trip = connections[current_con].trip;
        seg.service = trips[trip].service;
        seg.departure = connections[current_con].departure;
        seg.arrival = search_arrival(trips[trip].connections, FT, connections);
//        seg.arrival = seg.departure + 20;
        seg.from = current_sta;
        seg.to = FT;
        path.push_back(seg);
        current_sta = FT;
        current_time = path.back().arrival + stations[current_sta].transfer_time;
    } while (FT != target);
    return path;
}

void print_path(const vector<Segment> &path, const vector<Station> &stations, const vector<string> &services) {
    if (path.at(0).from == -1) {
        cout << "\nNo path found" << endl;
    } else {
        cout << endl;
        for (const auto &s: path) {
            cout << "'" << stations.at(s.from).name << "' --> '" << stations.at(s.to).name << "'  dep. @ "
                 << setfill('0') << setw(2) << s.departure / 60 <<
                 ":" << setfill('0') << setw(2) << s.departure % 60 << "  & arr. @ " << setfill('0') << setw(2)
                 << s.arrival / 60 << ":" << setfill('0') << setw(2)
                 << s.arrival % 60 << "  via '" << services.at(s.service) << "' service" << endl;
        }
    }
}

double compute_size(const vector<vector<vector<Label>>> &labels) {
    double size{0};
    for (int s{0}; s < labels.size(); s++) {
        for (int t{0}; t < labels[s].size(); t++) {
            size += labels[s][t].size() * 2 * 2; // 2 short integers with 2 bytes each
        }
    }
    return (size / 1000000);
}




// first step of label compression is still possible. However, second step is deleted as it is inefficient.
//vector <Label> compress_labels (const vector <Label> &labels){
//    vector <Label> labels_compressed;
//    for (int i{0}; i < labels.size();) {
//        Label compressed = labels.at(i);
//        for (int j {i+1}; ; j++){
//            if (j != labels.size() && labels.at(j).target == labels.at(i).target && labels.at(j).service == labels.at(i).service &&
//                labels.at(j).first_transfer == labels.at(i).first_transfer){
//                compressed.departures.push_back(labels.at(j).departures.at(0));
//                compressed.EAs.push_back(labels.at(j).EAs.at(0));
//                compressed.t_times.push_back(labels.at(j).t_times.at(0));
//            } else{
//                labels_compressed.push_back(compressed);
//                i = j;
//                break;
//            }
//        }
//    }
//    return labels_compressed;
//}


//    vector<vector<Station_Label>> Station_Labels(all_stations.size());
//    for (int s{0}; s < Station_Labels.size(); s++) {
//        Station_Label SL0;
//        Station_Labels.at(s).push_back(SL0);
//        int no{0};
//        for (int l{0}; l < all_labels.at(s).size(); l++) {
//            if (l == all_labels.at(s).size() - 1 || all_labels.at(s).at(l).target == all_labels.at(s).at(l + 1).target) {
//                if (all_labels.at(s).at(l).trip != -1) {
//                    Station_Labels.at(s).at(no).departures.push_back(all_labels.at(s).at(l).departure);
//                    Station_Labels.at(s).at(no).trips.push_back(all_labels.at(s).at(l).trip);
//                    Station_Labels.at(s).at(no).FTs.push_back(all_labels.at(s).at(l).FT);
//                }
//            } else {
//                if (all_labels.at(s).at(l).trip != -1) {
//                    Station_Labels.at(s).at(no).departures.push_back(all_labels.at(s).at(l).departure);
//                    Station_Labels.at(s).at(no).trips.push_back(all_labels.at(s).at(l).trip);
//                    Station_Labels.at(s).at(no).FTs.push_back(all_labels.at(s).at(l).FT);
//                }
//                no++;
//                Station_Label SL;
//                Station_Labels.at(s).push_back(SL);
//            }
//        }
//    }



//void create_labels(const int &source, const vector<Station> &stations, const vector<Connection> &connections,
//                   const int &trips_no, vector<vector<vector<Label>>> &Labels) {
//    vector<vector<Tempo_Label>> tempo(stations.size());
//    for (int d{0}; d < stations[source].depEvents.size(); d++) {
//        int c0 = stations[source].depEvents[d]; // first connection to start from
//        int dep = connections[c0].departure;
//        int trip = connections[c0].trip;
//        int cn = first_con(connections, dep + 220); // last connection / can be modified using connection-index
//        vector<int> EA(stations.size(), INT_MAX);
//        vector<int> FT(stations.size(), INT_MAX);
//        vector<bool> is_reachable(trips_no + 1, false);
//        vector<int> trip_station(trips_no + 1, -1);
//        EA.at(source) = dep;
//        is_reachable.at(trip) = true;
//        trip_station.at(trip) = source;
//        FT[source] = -1;
//        for (int i{c0}; i <= cn; i++) {
//            if (connections[i].departure > dep && connections[i].to != source && connections[i].from != source &&
//                (is_reachable[connections[i].trip] ||
//                 connections[i].departure -
//                 stations[connections[i].from].transfer_time >=
//                 EA[connections[i].from]) || i == c0) {
//                if (!is_reachable[connections[i].trip]) {
//                    is_reachable[connections[i].trip] = true;
//                    trip_station[connections[i].trip] = connections[i].from;
//                }
//                if (connections[i].arrival < EA[connections[i].to]) {
//                    EA[connections[i].to] = connections[i].arrival;
//                    if (connections[i].trip == trip) {
//                        FT[connections[i].to] = connections[i].to;
//                    } else {
//                        FT[connections[i].to] = FT[trip_station[connections[i].trip]];
//                    }
//                }
//            }
//        }
//        // on-the-fly domination check (filtration) and create temporary labels
//        for (int sta{0}; sta < stations.size(); sta++) {
//            if (sta != source && EA[sta] != INT_MAX && (tempo[sta].empty() || EA[sta] < tempo[sta].back().EA)) {
//                Tempo_Label tl(c0, dep, FT[sta], EA[sta]);
//                if (tempo[sta].empty() || dep < tempo[sta].back().dep) {
//                    tempo[sta].push_back(tl);
//                } else if (dep == tempo[sta].back().dep) {
//                    tempo[sta].back() = tl;
//                }
//            }
//        }
//
//        EA.clear();
//        EA.shrink_to_fit();
//        is_reachable.clear();
//        is_reachable.shrink_to_fit();
//        trip_station.clear();
//        trip_station.shrink_to_fit();
//        FT.clear();
//        FT.shrink_to_fit();
//    }
//    // create permanent labels
//    for (int s{0}; s < stations.size(); s++) {
//        if (!tempo[s].empty()) {
//            reverse(tempo[s].begin(), tempo[s].end());
//        }
//        for (int i{0}; i < tempo[s].size(); i++) {
//            Label l(tempo[s][i].con, tempo[s][i].FT);
//            Labels[source][s].push_back(l);
//        }
//    }
//    tempo.clear();
//    tempo.shrink_to_fit();
//}