//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_LABEL_H
#define PTVFTCSA2_LABEL_H

#include <cstdint>

class Label {
public:
    uint16_t connection;
    uint16_t FT;

    Label();
    ~Label();
    Label(uint16_t connection_val, uint16_t FT_val);
};


#endif //PTVFTCSA2_LABEL_H
