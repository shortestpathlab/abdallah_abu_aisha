//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_STATION_LABEL_H
#define PTVFTCSA2_STATION_LABEL_H

#include <iostream>
#include <vector>

using namespace std;

class Tempo_Label {
public:
    int con;
    int dep;
    int FT;
    int EA;

    Tempo_Label();
    ~Tempo_Label();
    Tempo_Label(int con_val, int dep_val, int FT_val, int EA_val);
};


#endif //PTVFTCSA2_STATION_LABEL_H
