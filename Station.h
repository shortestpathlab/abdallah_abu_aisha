//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_STATION_H
#define PTVFTCSA2_STATION_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Station {
public:
    string name;
    vector<int> depEvents; // connection indices
    int transfer_time;

    Station();
    ~Station();
    Station(string name_val, vector<int> depEvents_val, int transfer_time_val);
};


#endif //PTVFTCSA2_STATION_H
