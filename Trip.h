//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_TRIP_H
#define PTVFTCSA2_TRIP_H

#include <iostream>
#include <vector>

using namespace std;

class Trip {
public:
    vector<int> connections;
    int service;

    Trip();
    ~Trip();
    Trip(vector<int> connections_val, int service_val);
};


#endif //PTVFTCSA2_TRIP_H
