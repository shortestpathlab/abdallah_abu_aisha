//
// Created by Abdallah Abu Aisha on 13/9/2022.
//

#ifndef PTVFTCSA2_SEGMENT_H
#define PTVFTCSA2_SEGMENT_H


class Segment {
public:
    int from;
    int to;
    int departure;
    int arrival;
    int service;

    Segment();
    ~Segment();
    Segment(int from_val, int to_val, int departure_val, int arrival_val, int service_val);
};


#endif //PTVFTCSA2_SEGMENT_H
